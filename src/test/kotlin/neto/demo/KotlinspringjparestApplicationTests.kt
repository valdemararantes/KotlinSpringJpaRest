package neto.demo

import io.kotlintest.specs.FeatureSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestContextManager

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@DirtiesContext
class KotlinspringjparestApplicationTests : FeatureSpec() {
    @Autowired
    val restTemplate: TestRestTemplate? = null

    @Autowired
    val repository: PersonRepository? = null


    override fun beforeAll() {
        TestContextManager(this.javaClass).prepareTestInstance(this)
    }

    init {
        feature("People endpoint") {
            scenario("Asserting getting a person is working") {
                repository?.save(Person(name = "Gabriel", age = 23))
                val entity = restTemplate?.getForEntity("/people/1", Person::class.java)
                entity?.statusCodeValue shouldBe 200
            }
        }
    }
}
