package neto.demo

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.repository.PagingAndSortingRepository
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import org.springframework.data.rest.core.annotation.RepositoryRestResource as Resource

@SpringBootApplication
class KotlinspringjparestApplication

fun main(args: Array<String>) {
    SpringApplication.run(KotlinspringjparestApplication::class.java, *args)
}

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "people")
data class Person(@Id @GeneratedValue val id: Long? = null, val name: String? = null, val age: Int? = null)

@Resource(collectionResourceRel = "people", path = "people")
interface PersonRepository : PagingAndSortingRepository<Person, Long>

