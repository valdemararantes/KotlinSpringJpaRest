package neto.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong


/**
 * Created by Valdemar on 02/01/2017.
 */

data class Greeting(val id: Long, val content: String)

@RestController
class GreetingController {
    private val template = "Hello, %s!"
    private val counter = AtomicLong()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(required = false, defaultValue = "World") name: String): Greeting {
        println("==== in greeting ${counter.incrementAndGet()} ====")
        if (name == "exception") throw Exception("Forced Exception");
        return Greeting(counter.get(), String.format(template, name))
    }

}
